use std::io::{self};

use differential_dataflow::input::Input;

use differential_dataflow::operators::Group;
use differential_dataflow::algorithms::prefix_sum::PrefixSum;

fn main() -> io::Result<()> {

    // Expect the input file filename as the first argument, timely args after.
    let input_filename = std::env::args().nth(1).unwrap();

    let input = std::fs::read_to_string(&input_filename)
        .expect("Something went wrong reading the file");

    timely::execute_from_args(
        std::env::args().skip(1), move |worker| {

            let index = worker.index();
            let peers = worker.peers();

            let input_lines = input.lines()
                .collect::<Vec<_>>();

            let input_len = input_lines.len();

            let worker_input = input_lines.iter()
                .enumerate()
                .filter(|&(pos, _)| pos % peers == index)
                .map(|(i, s)| (i, s.to_string()))
                .collect::<Vec<_>>();

            // Part 1
            let (mut input, probe, probe2) = worker.dataflow::<(),_,_>(move |scope| {

                // Create a collection from the input
                let (input, data) = scope.new_collection_from(worker_input.clone());

                let psums = data
                // Parse the Strings to ints
                    .map(|(i, s)| (i, s.parse::<i32>().unwrap()))
                // prefix_sum needs to be of the form ((id, key), data)
                    .map(|(pos, x)| ((pos, ()), x))
                // do the parallel divide and conquer pairwise addition
                    .prefix_sum(0, |_key, x, y| *x + *y);

                let probe = psums
                // Grab the correct element
                    .filter(move |x| {((x.0).0) == input_len})
                // print output
                    .inspect(|x| println!("psum: {:?}", x))
                    .inspect(|x| println!("Total: {:?}", ((x.0).1)))
                    .probe();

                let probe2 = psums.map(|((i, _k), s)| (s, i))
                    .group(|_k, input, output|{ 
                        let input_len = input.len();
                        let mut sorted = vec![];
                        if input_len > 1 {
                            for idx in 0..input_len {
                                let (&input_idx, _count) = input[idx];
                                match sorted.binary_search(&input_idx) {
                                    Ok(_pos) => {}
                                    Err(pos) => {
                                        sorted.insert(pos, input_idx);
                                    }
                                }

                            }
                            if sorted.len() > 2 {
                                output.push((sorted[1], input_len as i32));
                            }
                            else {
                                output.push((sorted[sorted.len() - 1], input_len as i32));
                            }
                        }
                    })
                    .map(|(_, i)| {
                        println!("i: {:?}", i);
                        (1, i)
                    })
                    .group(|_k, input, output| {
                        println!("input: {:?}", input);
                        let input_len = input.len();
                        let mut min_idx = std::usize::MAX;
                        for idx in 0..input_len {
                            let (&input_idx, _count) = input[idx];
                            if input_idx < min_idx {
                                min_idx = input_idx;
                            }
                        }
                        output.push((min_idx, input_len as i32));

                    })
                    .inspect(|x| println!("x: {:?}", x))

                    .probe();
                (input, probe, probe2)
            });

            // Needed to make sure the last element is processed?
            input.insert((input_len, "0".to_string()));
            input.flush();

            while probe.less_than(input.time()) {
                worker.step();
            }

            while probe2.less_than(input.time()) {
                worker.step();
            }


        }).unwrap();
    Ok(())
}
